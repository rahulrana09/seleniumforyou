package scripts.seTests;


import com.aventstack.extentreports.ExtentTest;
import framework.utility.common.Assertion;
import framework.utility.common.DriverFactory;
import framework.utility.common.Utils;
import org.testng.annotations.Test;
import scripts.TestInit;

public class TrainingTest_01 extends TestInit {

    @Test(priority = 1, groups = {"SET_1"}, enabled = true)
    public void TC_FEATURE_01() {
        ExtentTest t1 = pNode.createNode("TC_FEATURE_01", "TC_FEATURE_01");
        try {
            DriverFactory.getDriver().get("https://google.com");
            Utils.maxWait();
            Utils.captureScreen(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {"SET_2"})
    public void TC_FEATURE_02() {
        ExtentTest t1 = pNode.createNode("TC_FEATURE_02", "TC_FEATURE_02");
        try {
            DriverFactory.getDriver().get("https://facebook.com");
            Utils.maxWait();
            Utils.captureScreen(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

}
