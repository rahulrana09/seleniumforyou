package framework.pageObjects.loginManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends PageInit {

    @FindBy(id = "username")
    WebElement txtUserName;


    public LoginPage(ExtentTest t1) {
        super(t1);
    }

    public static LoginPage init(ExtentTest t1) {
        return new LoginPage(t1);
    }

}
