package framework.utility.globalConst;

import framework.utility.propertiesManager.AutProperties;
import org.testng.Assert;

public class ConfigInput {
    public static String browser,
            language,
            country,
            defaultCountry,
            url,
            autAdminId,
            autAdminPwd,
            autUserId,
            autUserPwd,
            lastUser,
            suiteName;

    public static boolean isAssert, isConfirm, isCloudExecution, runHeadless = false;

    public static int additionalWaitTime;

    /**
     * C O N F I G U R A T I O N   I N P U T
     */
    public static void init() {

        try {
            AutProperties Me = AutProperties.getInstance("automation.properties");
            if (System.getProperty("browser") == null)
                browser = Me.getProperty("browser.name");
            else
                browser = System.getProperty("browser");

            if (System.getProperty("runHeadless") != null)
                runHeadless = System.getProperty("runHeadless").equalsIgnoreCase("true") ? true : false;

            // Localization code
            language = Me.getProperty("locale.language");
            country = Me.getProperty("locale.country");
            defaultCountry = Me.getProperty("default.country");
            isCloudExecution = Me.getProperty("is.cloud").equalsIgnoreCase("true") ? true : false;


            // application url
            url = Me.getProperty("app.url");

            if (browser.equalsIgnoreCase("ie")) {
                // sync config for Ie
                additionalWaitTime = 500;
            } else {
                //sync config for Rest of browsers
                additionalWaitTime = 250;
            }

            isAssert = true;
            isConfirm = true;

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Some thing went wrong, check automation.properties File");
        }
    }

}
