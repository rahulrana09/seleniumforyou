package framework.utility.common;

import framework.utility.globalConst.ConfigInput;
import framework.utility.globalConst.Constants;
import framework.utility.globalConst.FilePath;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class DriverFactory {

    private static final String CHROME = "chrome";
    public static final String IE = "ie";
    private static final String DEFAULT = "firefox";
    public static WebDriver driver;
    private static Logger logger = LoggerFactory.getLogger(DriverFactory.class);

    public static WebDriver getDriver() {
        if (driver == null) {
            driver = createDriver();
            driver.manage().timeouts().implicitlyWait(Constants.IMPLICIT_WAIT_TIME, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
        return driver;
    }

    public static WebDriver createDriver() {
        logger.info("Awesome! Let's create the Selenium WebDriver...");
        try {
            if (ConfigInput.browser.equalsIgnoreCase(CHROME)) {
                System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

                // preferences
                HashMap<String, Object> chromePrefs = new HashMap<>();
                chromePrefs.put("download.default_directory", FilePath.dirFileDownloads);
                chromePrefs.put("profile.default_content_settings.popups", 0);
                chromePrefs.put("profile.default_content_setting_values.notifications", 2);

                // options
                ChromeOptions options = new ChromeOptions();

                if (ConfigInput.runHeadless) {
                    options.addArguments("--headless");
                    options.addArguments("--window-size=1280,720");
                }

                options.setExperimentalOption("prefs", chromePrefs);
                options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
                options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                options.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
                options.setCapability(ChromeOptions.CAPABILITY, options);
                options.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, false);
                return new ChromeDriver(options);
            } else if (ConfigInput.browser.equalsIgnoreCase(IE)) {
                /**
                 * IE Browser
                 */
                System.setProperty("webdriver.edge.driver", "drivers/msedgedriver.exe");
                return new EdgeDriver();
            } else {
                Assertion.markAsFailure("Provide correct Browser Type");
            }

        } catch (Exception e) {
            Assertion.markAsFailure(e.getMessage());
        }
        return null;
    }

    public static String getCurrentTitle() {
        return driver.getTitle();
    }

    public static void closeDriver() {
        if (driver == null)
            return;

        driver.close();
        driver = null;
    }

    public static void quitDriver() {
        if (driver == null)
            return;

        driver.quit();
        driver = null;
    }

}
