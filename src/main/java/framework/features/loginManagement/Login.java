package framework.features.loginManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.User;
import framework.utility.common.Assertion;
import framework.utility.common.DataFactory;
import framework.utility.common.DriverFactory;
import framework.utility.common.Utils;
import framework.utility.globalConst.ConfigInput;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class Login {
    private static ExtentTest pNode;

    public static Login init(ExtentTest t1) {
        pNode = t1;
        return new Login();
    }

    /**
     * Login with specific user, if user is already logged in > return
     *
     * @param loginId
     * @throws IOException
     */
    public void login(String loginId, String homeTitle) throws Exception {
        Markup m = MarkupHelper.createLabel("Login as: " + loginId, ExtentColor.BLUE);
        pNode.info(m);

        User autUser = DataFactory.getAutUser(loginId);

        // check if user is already logged in with current View
        if (ConfigInput.lastUser != null && ConfigInput.lastUser.equalsIgnoreCase(autUser.loginId)) {
            pNode.pass("Already Logged in as User: " + loginId);
            return;
        }

        if (ConfigInput.lastUser != null) {
            logOut();
        }

        try {
            WebDriver driver = DriverFactory.getDriver();
            driver.get(ConfigInput.url);
            pNode.info("Open Application, URL: " + ConfigInput.url);

            // todo login code goes here

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * Log out from application, close any additional Tabs
     *
     * @throws Exception
     */
    public void logOut() throws Exception {
        try {
            if (ConfigInput.lastUser == null) {
                pNode.info("No Last User Logged In!");
                return;
            }

            // todo logout code goes here
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        } finally {
            Utils.captureScreen(pNode);
            ConfigInput.lastUser = null;
            DriverFactory.closeDriver();
        }
    }


}
